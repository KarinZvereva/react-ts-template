const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

require('dotenv').config()

const devMode = process.env.NODE_ENV !== 'production'

const DEVSERVER_PORT = process.env.DEVSERVER_PORT || 8080

const OUTPUT_DIR = path.resolve(__dirname, 'dist')
const APP_DIR = path.resolve(__dirname, 'src')

const plugins = [
    new HtmlWebpackPlugin({
        template: path.join(__dirname, 'src', 'index.html')
    }),
    new MiniCssExtractPlugin()
]

module.exports = {
    mode: devMode ? 'development' : 'production',

    context: APP_DIR,

    entry: {
        app: 'index.tsx'
      },

    target: 'web',

    resolve: {
        modules: [APP_DIR, 'node_modules'],
        extensions: ['.tsx', '.ts', '.js', '.jsx', '.json'],
    },
    
    stats: {
        warningsFilter: /export .* was not found in/,
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: '/node_modules/'
            },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
        ],
    },
    
    output: {
        filename: '[name].js',
        path: OUTPUT_DIR,
    },
    
    plugins,

    devtool: devMode ? 'source-map' : false,

  devServer: devMode
    ? {
    historyApiFallback: true,
      hot: true,
      disableHostCheck: true,
      inline: true,
      headers: { 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': '*' },
      port: DEVSERVER_PORT,
    }
    : {},
}
